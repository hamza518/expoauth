import { createSlice, nanoid, createAsyncThunk } from "@reduxjs/toolkit";
import axios from "axios";
import { Alert } from "react-native";
import User from "../../data/User";
import Urls, { BASEURL } from "../../server/Urls";
import AsyncStorage from '@react-native-async-storage/async-storage';


export const loginUser = createAsyncThunk(
  "user/loginStatus",
  async({email,password}, thunkAPI)=>{
    try{
      const formdata = new FormData();
        formdata.append("email", email);
        formdata.append("password", password);
        const response = await axios.post(`${BASEURL}${Urls.LOGIN}`, formdata)
    
    if (response.data == "failed") {
      Alert.alert( 
        "Alert!",
        "wrong credentials!",
        [{text: 'Ok', style: 'default', onPress:()=> {
          thunkAPI.dispatch(loginFailed())
        }}]
      );
    }else{
      thunkAPI.dispatch(login(response.data));
    }
    }catch(err){
      Alert.alert( 
        "Alert!",
        "something went wrong!",
        [{text: 'Ok', style: 'default'}]
      );
    }
  }
)

const AuthSlice = createSlice({
  name: 'auth',
  initialState: { User: { name: 'ham', id: nanoid(), login: false} },
  reducers: {
    login: (state, action) => {
      const {id, name,} = action.payload; 

      // if(action.payload?.status === "1"){
        const user = new User(id, name, true)
        AsyncStorage.setItem('user_data', JSON.stringify(user));
        return user  // mutate the state all you want with immer
        
      // }
    },
    loginFailed: () =>{
      return { User: { name: '', id: '', login: false} }
    },
    logout: (state, action) =>{
      const user = new User( nanoid(), "ham", action.payload)
        return user 
    }
  },
  extraReducers: {
    // Add reducers for additional action types here, and handle loading state as needed
    [loginUser.rejected] : () =>{
      Alert.alert( 
        "Alert!",
        "something went wrong",
        [{text: 'Ok', style: 'default'}]
      );
    }
  }
})

export const { login, logout, loginFailed } = AuthSlice.actions;

export default AuthSlice.reducer;