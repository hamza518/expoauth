import React, { useCallback, useEffect, useState } from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack'
import LoginScreen from '../screens/LoginScreen';
import BottomTab from './BottomTab';
import { useSelector } from 'react-redux';
import AsyncStorage from '@react-native-async-storage/async-storage';
import BlogScreen from '../screens/BlogScreen';

const MainStack = () =>{
    const Stack = createStackNavigator();
    const {login} = useSelector(state => state.root);
    const [ loginUser, setLoginUser ] = useState(login);

    const getData =useCallback (async() =>{
        const data = await AsyncStorage.getItem("user_data");
        user = data != null && JSON.parse(data);
        user != null && setLoginUser(user?.login)
    },[AsyncStorage, loginUser])

    useEffect(()=>{
        setLoginUser(login)
    }, [login])
    
    useEffect(()=>{
        getData()
    }, [getData])

    return(
        
        <NavigationContainer>
            <Stack.Navigator>
               {   !loginUser ?
                    <Stack.Screen
                    options={{
                        headerShown: false
                    }}
                        name="Login"
                        component={LoginScreen}
                    /> :
                <>
                    <Stack.Screen
                        options={{
                            headerShown: false
                        }}
                        name="Home"
                        component={BottomTab}
                    />
                    <Stack.Screen
                        options={{
                            headerShown: false
                        }}
                        name="Blog"
                        component={BlogScreen}
                    />
                </>
            }
            </Stack.Navigator>
        </NavigationContainer>
    )

}

export default MainStack;