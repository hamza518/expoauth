import AsyncStorage from "@react-native-async-storage/async-storage";
import React from "react";
import {View , Text, StyleSheet, TouchableOpacity} from "react-native";
import { useDispatch } from "react-redux";
import Colors from "../constants/Colors";
import { logout } from "../store/slices/AuthSlice";
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen'

const LogoutScreen = ({ navigation }) =>{
    const dispatch = useDispatch();
    return(
        <View style={styles.container}>
            <Text style={styles.title}>DBazzar</Text>
            <TouchableOpacity style={styles.btnStyle} activeOpacity={0.6}
                onPress={()=>{
                    dispatch(logout(false))
                    AsyncStorage.removeItem("user_data");
                }}
            >
                <Text style={styles.textStyle}>Logout</Text>
            </TouchableOpacity>
        </View>
    )
}

const styles = StyleSheet.create({
    container:{
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    title: {
        color: Colors.redDark,
        fontSize: wp(12),
        marginBottom: hp(12)
    },
    btnStyle :{
        backgroundColor: Colors.redLight,
        width: wp("80%"),
        height: 50,
        borderRadius: 5,
        justifyContent: 'center',
        alignItems: 'center'
    },
    textStyle: {
        color: Colors.grayLight,
        fontSize: wp(5),
        fontWeight: 'bold'
    }
});

export default LogoutScreen;