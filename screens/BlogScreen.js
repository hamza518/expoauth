import React from "react";
import {View, StyleSheet, Text, SafeAreaView, Platform } from "react-native";
import { WebView } from 'react-native-webview';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import Icon from "react-native-vector-icons/FontAwesome5"
import Colors from "../constants/Colors";

const BlogScreen = ({navigation, route}) =>{
    const {blogData} = route.params;

    return(
        <SafeAreaView style={styles.screen}>
            <View style={styles.container}>
                <Icon name="chevron-left" size={wp(5)} 
                    style={{marginLeft: 10, marginTop: 10}}
                    onPress={()=>{
                        navigation.goBack();
                    }} 
                />
                <View style={styles.header}>
                    <Text style={{...styles.blogHeading, fontSize: wp(6), marginTop: 20}}>{blogData.heading}</Text>
                    <Text style={{...styles.blogHeading, fontSize: wp(2.8)}}>{blogData.sub_heading}</Text>
                </View>
                <WebView
                    source={{ html: blogData.blog }}
                    style={styles.blogContainer}
                />
        </View>
      </SafeAreaView>
    )    
}

const styles = StyleSheet.create({
    screen: {
        flex: 1,
        marginTop: Platform.OS === "android" ? 25 : 0,
        backgroundColor: Colors.white
    },
    header: {
        width: wp("100%"),
        alignItems: "center",
        backgroundColor: Colors.white
    },
    container: {
        flex: 1,
    },
    blogHeading: {
        marginBottom: 20,
        fontWeight: "bold",
    },
});

export default BlogScreen;