import React, { useCallback, useEffect, useState } from 'react';
import { KeyboardAvoidingView, ScrollView, View, Text, FlatList, StyleSheet, TouchableOpacity, RefreshControl, Image, SafeAreaView, Platform} from 'react-native'
import Colors from '../constants/Colors';
import Icon from 'react-native-vector-icons/FontAwesome5'
import { useSelector, useDispatch } from 'react-redux';
import { login } from '../store/slices/AuthSlice';
import AdCard from '../components/AdCard';
import ServiceCard from '../components/ServiceCard';
import { getDashboardData } from '../store/slices/DashboardSlice';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen'
import AsyncStorage from '@react-native-async-storage/async-storage';

const wait = (timeout) => {
    return new Promise(resolve => setTimeout(resolve, timeout));
}

const HomeScreen = ({navigation})=>{
    let homeUser = useSelector(state=> state.root)
    const dashboardState = useSelector(state => state.dashboard)
    const dispatch = useDispatch();

    let displayArray;

    const [homeState, setHomeState] = useState(homeUser);
    const [refreshing, setRefreshing] = React.useState(false);

    const [loader, setLoader] = useState(false)
    const [seeMore, setSeeMore] = useState(false);

    let categories = [];
    for(const key in dashboardState){
        categories.push(dashboardState[key])
    }

    const blogs =  dashboardState?.blog;

    if(!seeMore){
        displayArray = categories.length > 0 && [...categories[0].slice(0,7),{name:"LoadMore", icon: "https://cdn.webshopapp.com/shops/259039/files/222985577/korean-felt-3-mm-dark-red.jpg"}] 
    }else{
        displayArray = [...categories[0],{name:'HideAll', icon: "https://cdn.webshopapp.com/shops/259039/files/222985577/korean-felt-3-mm-dark-red.jpg"}]
    }


    const onRefresh = React.useCallback(() => {
        getDashboardDataHandler(homeState.id);
        setRefreshing(true);
        wait(2000).then(() => setRefreshing(false));
    }, [getDashboardDataHandler]);

    const getDashboardDataHandler = useCallback(async(id) =>{
        dispatch(getDashboardData(id))
    },[getDashboardData]);

    const getData = useCallback(async() =>{
        const data = await AsyncStorage.getItem("user_data");
        const user = data != null ? JSON.parse(data) : null;
        user != null && setHomeState(user)
    }, [AsyncStorage])

    useEffect(()=>{
        getDashboardDataHandler(homeState.id);
    }, [getDashboardDataHandler, homeState])

    useEffect(()=>{
        getData()
    }, [getData])

    useEffect(()=>{
        homeState === undefined ?
            setLoader(true) : setLoader(false)
    },[loader, homeState])
    
return(
    loader ? 
         <ActivityIndicator size="large" style={styles.loaderStyle} color={Colors.gray} />
         :
    <SafeAreaView style={styles.screen}>
        <ScrollView style={{flex: 1, marginBottom: 10}}
            refreshControl={
                <RefreshControl
                    refreshing={refreshing}
                    onRefresh={onRefresh}
                />
            }
        >
        <View style={styles.container}>
            <View style={styles.containerBox}>

                <View style={styles.searchbar}>
                    <TouchableOpacity style={styles.searchBtn}
                        onPress={()=>{ 
                            dispatch(login(false))
                        }}
                    >
                        <Icon name="circle" size={35} color="black" />
                        <Text style={styles.searchBtnText}>pay</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.searchInputBox} activeOpacity={0.6}>
                        <Icon name="search" style={styles.searchIcon} size={25} color={Colors.gray} />
                        <Text style={styles.input}> Search within DBazzar </Text>
                    </TouchableOpacity>
                </View>

                <View style={styles.userDetailBox}>
                    <View style={styles.userDetailBoxRow}>
                        <Text><Text style={styles.boldText}>Hi {homeState.name},</Text> you have</Text>
                        <Text style={styles.boldText} >RM 1285.21</Text>
                    </View>
                    <View style={styles.userDetailBoxRow}>
                        <Text style={styles.boldText}>214,674</Text>
                        <Text style={styles.boldText}>0 unused vouchers</Text>
                    </View>
                </View>
            </View>
        </View>
        <View style={styles.flatlistBox}>
            <FlatList
                data={displayArray}
                renderItem={({item})=>{
                    return <ServiceCard 
                    source={item.icon}
                    title={
                        item.name
                    }
                    style={styles.serviceBox}
                    onPress={()=>{
                        item.name === "LoadMore"  && setSeeMore(true)
                        item.name === "HideAll"  && setSeeMore(false)
                    }}
                />
                }}
                numColumns={4}
                keyExtractor={(item, index)=> item + index}
            />
        </View>
        <View style={styles.adContainer}>
            <FlatList
                data={blogs}
                renderItem={({item})=>{
                    return <AdCard
                    source={item.header}
                    title={item.heading}
                    subTitle={item.sub_heading}
                    style={styles.adBox}
                    onPress={()=>{navigation.navigate("Blog", {blogData: item})}}
            />
            }}
            numColumns={2}
            />
        </View>
        </ScrollView>
    </SafeAreaView>
)

}

const styles = StyleSheet.create({
    screen: {
        paddingTop: Platform.OS === "android" ? 25 : 0,
        flex: 1
    },
    container: {
        flex: 1,
    },
    containerBox: {
        height: hp("30%"),
        backgroundColor: Colors.redLight,
        paddingHorizontal: 20,
    },
    searchbar: {
        width: wp("90%"),
        height: 60,
        borderRadius: 5,
        overflow: "hidden",
        flexDirection: "row",
        marginVertical: 20,
        backgroundColor: Colors.white,
    },
    searchBtn: {
        backgroundColor: Colors.grayLight,
        paddingHorizontal: 10,
        paddingTop: 3,
        width: 55,
        height: 60
     },
     searchBtnText: {
        textAlign: "center",
     },
     searchInputBox: {
        alignItems: "center",
        flexDirection: "row",
        paddingLeft: 40
     },
     userDetailBox: {
         backgroundColor: Colors.white,
         borderRadius: 5
     },
     userDetailBoxRow: {
         flexDirection: "row",
         justifyContent: "space-between",
         margin: 10
     },
     boldText: {
         fontWeight: "bold",
         fontSize: 13
     },
     adContainer: {
        marginBottom: 20,
        alignItems: "center"

     },
     adBox: {
        width: wp("47%"),
        height: hp("50%"),
        alignItems: "center"
     },
     serviceBox: {
         width: 90,
         height: 90,
         margin: 5
     },
     flatlistBox: {
         paddingBottom: 10
     },
     footerBtn: {
         width: 70, 
         height: 70, 
         borderRadius: 35, 
         backgroundColor: Colors.redDark,
         paddingLeft: 13,
         justifyContent: 'center'
    }

})

export default HomeScreen