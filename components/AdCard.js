import React from "react";
import {View , Text, Image, StyleSheet, TouchableOpacity} from "react-native";
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import Colors from "../constants/Colors";

const AdCard = ({source, title, subTitle, style, onPress}) =>{
    return(
        <View  style={{ ...style}}>
            <TouchableOpacity style={styles.container} activeOpacity={0.6}
                onPress={onPress}
            >
                <Image source={{uri: source}} style={styles.imgStyle} />
                <Text style={styles.textStyle}
                    numberOfLines={1}
                >{title}</Text>
                <Text style={styles.subTextStyle}
                    numberOfLines={2}
                >{subTitle}</Text>
            </TouchableOpacity>
        </View>
    )    
}

const styles = StyleSheet.create({
    container:{
        
    },
    imgStyle: { 
        width: wp("45%"),
        height: hp("35%"),
        borderRadius: 10,
    }, 
    textStyle: {
        fontSize: wp(4),
        fontWeight: "bold",
        textAlign: 'left',
        color: Colors.gray,
        marginBottom: 20
    },
    subTextStyle: {
        color: Colors.gray
    }
});

export default AdCard;