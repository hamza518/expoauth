import React from "react";
import { Text, Image, StyleSheet, TouchableOpacity, View} from "react-native";
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen'

const ServiceCard = ({source, title, style, onPress}) =>{
    return(
        <View style={{ ...style}}> 
            <TouchableOpacity style={styles.container} activeOpacity={0.6}
                onPress={onPress}
            >
                <Image source={{uri: source}} style={styles.imgStyle} />
                <Text 
                    style={styles.textStyle}
                    numberOfLines={1}
                >
                        {title}
                    </Text>
            </TouchableOpacity>
        </View>
    )    
}

const styles = StyleSheet.create({
    container:{
        marginBottom: 5,
    },
    imgStyle: { 
        width: wp("20%"),
        height: hp("12%"),
        borderRadius: wp("10%"),
    }, 
    textStyle: {
        textAlign: 'center',
        fontSize: wp(3),


    }
});

export default ServiceCard;