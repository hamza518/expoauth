import { combineReducers, configureStore } from '@reduxjs/toolkit';
import React from 'react';
import { Provider } from 'react-redux';
import MainStack from './navigation/MainStack';
import ReduxThunk from "redux-thunk"
import { LogBox } from 'react-native';
import AuthSlice from './store/slices/AuthSlice';
import DashboardSlice from './store/slices/DashboardSlice';
import logger from 'redux-logger';

export default function App() {

  const mainReducer = combineReducers({
    root: AuthSlice,
    dashboard: DashboardSlice
  })
  const store = configureStore({
    reducer: mainReducer, 
    middleware: [ReduxThunk]
  })
  LogBox.ignoreAllLogs();
  return (
    <Provider store={store}>
      <MainStack/>
    </Provider>
  );
}